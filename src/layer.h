#pragma once

#include <stdlib.h>

#include "../lib/xalloc.h"

#include "neuron.h"

/**
 * Basic layer structure
 * @size 			Size of the array of neurons
 * @neuron_array	Array of neurons that the layer contains 
 */
struct layer {
	size_t size;
	void **neuron_array;	
};

/**
 * Initializes a new layer 
 * @returns	Newly allocated layer 
 */
struct layer *layer_init(size_t nb_neurons);

/**
 * Destroys a layer 
 * @layer	Layer to destroy
 */
void layer_free(struct layer *layer);
