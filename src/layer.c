#include "layer.h"

static void fill_neuron_array_null(void **neuron_array, size_t size) {
	for(size_t i = 0; i < size; ++i)
		*(neuron_array + i) = NULL;
}

//FIXME : Change to neuron array instead of void array ?
struct layer *layer_init(size_t nb_neurons) {
	struct layer *new_layer = xmalloc(sizeof(struct layer));
	void **neuron_array = xmalloc(sizeof(void *) * nb_neurons);
	fill_neuron_array_null(neuron_array, nb_neurons);

	new_layer->size = nb_neurons;
	new_layer->neuron_array = neuron_array;

	return new_layer;
}

void layer_free(struct layer *layer) {
	for(size_t neuron_index = 0; neuron_index < layer->size; ++neuron_index)
		if (layer->neuron_array[neuron_index])
			neuron_free(layer->neuron_array[neuron_index]);
	
	free(layer->neuron_array);
	free(layer);
}
