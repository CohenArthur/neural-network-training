#include "neuron.h"

struct neuron *neuron_init() {
	struct neuron *new_neuron = xmalloc(sizeof(struct neuron));
	return new_neuron;
}

void neuron_free(struct neuron *neuron) {
	free(neuron);
}

int neuron_relu(int value) {
	if (value > 0)
		return value;
	return 0;
}
