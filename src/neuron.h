#pragma once
#include <stdlib.h>
#include "../lib/xalloc.h"

/** 
 * Basic neuron structure
 * @activated 	Represents the state of the neuron, 0 or 1
 */
struct neuron {
	int activated;
};

/**
 * Initializes a new neuron
 * @returns 	Newly allocated neuron
 */
struct neuron *neuron_init();

/**
 * Destroys a neuron
 * @neuron		Neuron to destroy
 */
void neuron_free(struct neuron *neuron);

/**
 * ReLU function : f(a) = a if a > 0
 * @value 		Value to perform relu on
 * @returns 	Result of ReLU(value)
 */
int neuron_relu(int value);
