#pragma once

#include <stdlib.h>

#include "../lib/xalloc.h"

#include "layer.h"

/**
 * Basic network structure
 * @size 		Size of the array of layers 
 * @layer_array	Array of layer that the network contains 
 */
struct network {
	size_t size;
	void **layer_array;	
};

/**
 * Initializes a new network
 * @returns	Newly allocated network
 */
struct network *network_init(size_t nb_layers);

/**
 * Destroys a network 
 * @network	Network to destroy
 */
void network_free(struct network *network);
