#include "network.h"

static void fill_layer_array_null(void **layer_array, size_t size) {
	for(size_t i = 0; i < size; ++i)
		*(layer_array + i) = NULL;
}

//FIXME : Change to neuron array instead of void array ?
struct network *network_init(size_t nb_layers) {
	struct network *new_network = xmalloc(sizeof(struct layer));
	void **layer_array = xmalloc(sizeof(void *) * nb_layers);
	fill_layer_array_null(layer_array, nb_layers);

	new_network->size = nb_layers;
	new_network->layer_array = layer_array;

	return new_network;
}

void network_free(struct network *network) {
	for(size_t layer_index = 0; layer_index < network->size; ++layer_index)
		if (network->layer_array[layer_index])
			layer_free(network->layer_array[layer_index]);
	
	free(network->layer_array);
	free(network);
}
