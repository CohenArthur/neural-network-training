#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>

#include "lib/tdd.h"

#include "src/neuron.h"
#include "src/layer.h"
#include "src/network.h"

static void test_assert_neuron() {
	char *alert = "test_assert_neuron()";

	struct neuron *test_neuron = neuron_init();
	test_neuron->activated = VALUE_0;
	neuron_free(test_neuron);

	struct neuron **neuron_array = xmalloc(sizeof(struct neuron) * MAX_TEST_NB);
	
	for(size_t i = 0; i < MAX_TEST_NB / 2; ++i) {
		struct neuron *test = neuron_init();
		test->activated = VALUE_1;
		*(neuron_array + i) = test;
		progress(i, alert);
	}

	for(size_t i = 0; i < MAX_TEST_NB / 2; ++i) {
		progress(i, alert);
		assert_int((*(neuron_array + i))->activated, VALUE_1, alert);
		neuron_free(*(neuron_array + i));
	}

	free(neuron_array);

	success(alert);
}

static void test_memleak_neuron() {
	char *alert = "test_memleak_neuron()";

	struct neuron *test_neuron = neuron_init();
	test_neuron->activated = VALUE_0;
	neuron_free(test_neuron);

//FIXME : Change to void array
	//struct neuron **neuron_array = xmalloc(sizeof(struct neuron) * MAX_TEST_NB);
	void **neuron_array = xmalloc(sizeof(struct neuron) * MAX_TEST_NB);
	
	for(size_t i = 0; i < MAX_TEST_NB / 2; ++i) {
		struct neuron *test = neuron_init();
		*(neuron_array + i) = test;
		progress(i, alert);
	}

	for(size_t i = 0; i < MAX_TEST_NB / 2; ++i) {
		progress(i, alert);
		neuron_free(*(neuron_array + i));
	}

	free(neuron_array);
	success(alert);
}

static void test_relu_neuron() {
	char *alert = "test_relu_neuron()";

	srand(time(NULL));

	for(size_t i = 0; i < MAX_TEST_NB; ++i) {
		int value = rand();
		if (value > 0)
			assert_int(neuron_relu(value), value, alert);
		else
			assert_int(neuron_relu(value), 0, alert);

		progress(i, alert);
	}

	success(alert);
}

static void test_memleak_layer() {
	char *alert = "test_memleak_layer()";

	struct layer *test = layer_init(VALUE_1);
	layer_free(test);

	void **layers = xmalloc(sizeof(void *) * MAX_TEST_NB);

	for(size_t i = 0; i < MAX_TEST_NB; ++i) {
		struct layer *new = layer_init(VALUE_1);
		*(layers + i) = new;
		progress(i, alert);
	}

	for(size_t i = 0; i < MAX_TEST_NB; ++i) {
		layer_free(*(layers + i));
		progress(i, alert);
	}

	free(layers);
	success(alert);
}

static void test_memleak_network() {
	char *alert = "test_memleak_network()";

	struct network *test = network_init(VALUE_1);
	network_free(test);

	void **networks = xmalloc(sizeof(void *) * MAX_TEST_NB);

	for(size_t i = 0; i < MAX_TEST_NB; ++i) {
		struct network *new = network_init(VALUE_1);
		*(networks + i) = new;
		progress(i, alert);
	}

	for(size_t i = 0; i < MAX_TEST_NB; ++i) {
		network_free(*(networks + i));
		progress(i, alert);
	}

	free(networks);
	success(alert);
}

static void test_suite_neuron() {
	test_assert_neuron();
	test_memleak_neuron();
	test_relu_neuron();
}

static void test_suite_layer() {
	test_memleak_layer();
}

static void test_suite_network() {
	test_memleak_network();
}

static void test_suite() {
	test_suite_neuron();
	test_suite_layer();
	test_suite_network();
}

int main(void) {
	test_suite();
}
