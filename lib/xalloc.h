#include <stdlib.h>
#include <err.h>

/**
 * Wrapper to handle malloc errors
 * @size 	size of the arrays you want
 * @return 	pointer to the newly allocated element
 */
void *xmalloc(size_t size);

/**
 * Wrapper to handle calloc errors
 * @qty 	First argument of calloc, how many arrays you want
 * @size 	size of the arrays you want
 * @return 	pointer to the newly allocated element
 */
void *xcalloc(size_t qty, size_t size);
